package com.cos399.goalfind.test;

import com.cos399.goalfind.test.sensortest.*;

public class TestSensorsDataExchange {

	private static ColorSensorTester colorTester = null;
	private static TouchSensorTester touchTester = null;
	private static UltrasonicSensorTester ultrasonicTester = null;
	private static TestSensorsDataExchange de = null;
	
	private int colorID, colorRaw, color, ultrasonicDistance;
	private boolean leftTouch, rightTouch;
	private String colorName;
	
	private TestSensorsDataExchange() { }
	
	public TestSensorsDataExchange getInstance() {
		if (de == null) {
			de = new TestSensorsDataExchange();
			colorTester = ColorSensorTester.getInstance();
			//touchTester = TouchSensorTester.getInstance();
			//ultrasonicTester = UltrasonicSensorTester.getInstance();
		}
		
		return de;
	}

	public int getColorID() {
		return colorID;
	}

	public void setColorID(int colorID) {
		this.colorID = colorID;
	}

	public int getColorRaw() {
		return colorRaw;
	}

	public void setColorRaw(int colorRaw) {
		this.colorRaw = colorRaw;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public int getUltrasonicDistance() {
		return ultrasonicDistance;
	}

	public void setUltrasonicDistance(int ultrasonicDistance) {
		this.ultrasonicDistance = ultrasonicDistance;
	}

	public boolean isLeftTouch() {
		return leftTouch;
	}

	public void setLeftTouch(boolean leftTouch) {
		this.leftTouch = leftTouch;
	}

	public boolean isRightTouch() {
		return rightTouch;
	}

	public void setRightTouch(boolean rightTouch) {
		this.rightTouch = rightTouch;
	}

	public String getColorName() {
		return colorName;
	}

	public void setColorName(String colorName) {
		this.colorName = colorName;
	}
	
	
	
}
