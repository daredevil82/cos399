/*	Monitors and handles dual light sensors, front and rear.
 * 
 */
package com.cos399.sumo.sensors;

import com.cos399.sumo.utils.SumoDataExchange;

import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.addon.ColorHTSensor;
import lejos.nxt.SensorPort;


public class Light extends Thread {
	
	private static LightSensor ls = null;
	private static ColorHTSensor cs = null;
	private static SensorPort lightPort = null;
	private static SensorPort colorPort = null;
	private static SumoDataExchange de = null;
	
	private int lightCurVal = -1, colorCurVal = -1;
	
	private static Light light = null;
	
	private Light() { }
	
	/*	Two getInstance methods available.  First assumes that the sensor ports
	 * 	have already been set via the appropriate setter, then returns a new or
	 * 	existing instance of this object, depending on null value.  
	 * 
	 * 	The other explicitly sets the sensor ports for both sensors and calls the 
	 * 	first getInstance to return a new or existing instance. 
	 * 
	 */
	private static Light getInstance() {
		if (lightPort == null || colorPort == null) {
			throw new UnsupportedOperationException("Light or Color Ports invalid");	
		}
		
		else if (light == null) {
			ls = new LightSensor(lightPort);
			cs = new ColorHTSensor(colorPort);
			light = new Light();
			de = SumoDataExchange.getInstance();
			
			ls.setFloodlight(true);
		}
		
		return light;
		
	}
	
	public static Light getInstance(SensorPort lightPort, SensorPort colorPort) {
		setPorts(lightPort, colorPort);
		
		return getInstance();
	}
	
	public static void setPorts(SensorPort lightPort, SensorPort colorPort) {
		Light.lightPort = lightPort;
		Light.colorPort = colorPort;
	}
	
	public void setLightSensorPort(SensorPort port) {
		Light.lightPort = port;
	}

	public void setColorSensorPort(SensorPort port) {
		Light.colorPort = port;
	}
	
	@Override
	public void run() {
		
		while (true) {
			lightCurVal = ls.readValue();
			colorCurVal = cs.getColorID();
			
			if (lightCurVal <= 45) {
				de.setBoundaryDetected(true);
				de.setBoundaryDetectedLightSensor("light");
			} else if (colorCurVal == 7) {
				de.setBoundaryDetected(true);
				de.setBoundaryDetectedLightSensor("color");
				
			} else {
				de.setBoundaryDetected(false);
				de.setBoundaryDetectedLightSensor("");
			}
			 
			LCD.drawString("BoundDect: " + de.getBoundaryDetected(), 0, 2);
		}
	}
}
