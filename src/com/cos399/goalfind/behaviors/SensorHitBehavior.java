/*	Behavior class that activates when the Touch or
 * 	Ultrasonic sensors record a hit
 * 
 */

package com.cos399.goalfind.behaviors;

import com.cos399.goalfind.controller.PilotController;
import com.cos399.goalfind.dataexchange.DataExchange;

import lejos.robotics.subsumption.Behavior;

public class SensorHitBehavior implements Behavior {

	private DataExchange de = DataExchange.getInstance();
	private PilotController pc = PilotController.getInstance();
	
	@Override
	public boolean takeControl() {
		return de.getCmd() == 0;
	}

	@Override
	public void action() {
		
		pc.getPilot().stop();
		
		if (pc.getHeadLeft())
			pc.executeLeftRotation();
		else
			pc.executeRightRotation();
		
	}

	@Override
	public void suppress() {
		//nothing to suppress
		
	}

}
