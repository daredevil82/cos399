package com.cos399.sumo.test;

import com.cos399.sumo.SumoController;
import com.cos399.sumo.drive.Drive;
import com.cos399.sumo.utils.SumoProperties;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.util.Delay;

public class MotorTest {

	private static MotorTest mt = null;
	private static Drive testDrive = null;
	private int lineSelection = 2; 
	private int TIMEOUT = 1000;
	
	private MotorTest() { }
	
	public static MotorTest getInstance() {
		if (mt == null) {
			mt = new MotorTest();
			SumoProperties sp = SumoProperties.getInstance();
			testDrive = Drive.getInstance(sp.getLeftMotorPort(), sp.getRightMotorPort(), 
					sp.getCenterMotorPort(), sp.getMaxRotationSpeed()); 
		}
		
		return mt;
	}
	
	private void drawScreen() {
		LCD.clear();
		
		LCD.drawString("Motor Testing", 0, 0);
		LCD.drawString("> 1: Forward", 0, 2);
		LCD.drawString("  2: Backwards", 0, 3);
		LCD.drawString("  3: Rotate", 0, 4);
		LCD.drawString("  4: Exit" , 0, 5);
	}
	
	private void processInput() {
		int button = -1;
		while(!Button.ESCAPE.isDown()) {
			button = Button.waitForAnyPress();
			
			switch(button) {
				case 1: //enter
					switch(lineSelection) {
						case 2: //execute forward motor test
							testForward();
							testMotors();
							break;
						
						case 3: //execute backwards motor test
							testBackward();
							testMotors();
							break;
							
						case 4: //execute rotation test
							testRotation();
							testMotors();
							break;
							
						case 5: //exit back to main menu
							LCD.clear();
							SumoController.main(null);
							break;
					}
					break;
					
				case 2: //left button, move up list
					LCD.clear(0, lineSelection, 1);
					
					if (lineSelection == 2)
						lineSelection = 5;
					else
						lineSelection--;
					
					LCD.drawChar('>', 0, lineSelection);
					
					break;
					
				case 4: //right button, move down list
					LCD.clear(0, lineSelection, 1);
					
					if (lineSelection == 5)
						lineSelection = 2;
					else
						lineSelection++;
					
					LCD.drawChar('>', 0, lineSelection);
					
					break;
					
			}
		}
		
		LCD.clear();
		SumoController.main(null);
	}
	
	private void testForward() {
		LCD.clear();
		TIMEOUT = 1000;
		
		LCD.drawString("Forward- 10 sec", 0, 1);
		LCD.drawString("Motor Speed:\n" + testDrive.getSpeed(), 0, 3);
		testDrive.forward();
		
		Delay.msDelay(10000);
		
		testDrive.stop();
		
		Delay.msDelay(2000);
		
		LCD.clear(1);
		LCD.drawString("Forward- 50", 0, 1);
		testDrive.forward(50);
		
		testDrive.stop();
	
	}
	
	private void testBackward() {
		LCD.clear(1);
		LCD.clear(2);
		LCD.drawString("Backward- 500", 0, 1);
		LCD.drawString("Testing Forward", 0, 2);
		TIMEOUT = 1000;
		
		testDrive.backward(500);
		testDrive.stop();
		
		
		LCD.clear(1);
		LCD.drawString("Backward- unlimited", 0, 1);
		
		
		while (TIMEOUT > 0) {
			testDrive.backward();
			TIMEOUT--;
		}
		
		testDrive.stop();
		
		LCD.clear(1);
		LCD.clear(2);
		LCD.drawString("Testing Left\nMotor\nBackward", 0, 2);
		for (int i = 0; i < 500; i++) 
			testDrive.getLeftMotor().backward();
		
		testDrive.stop();
		
		LCD.clear(2);
		LCD.drawString("Testing Center\nMotor\nBackward", 0, 2);
		for (int i = 0; i < 500; i++)
			testDrive.getCenterMotor().backward();
		
		testDrive.stop();
		
		LCD.clear(2);
		LCD.drawString("Testing Right\nMotor\nBackward", 0, 2);
		for (int i = 0; i <500; i++)
			testDrive.getRightMotor().backward();
		
		
		testDrive.stop();
				
		
	}
	 
	private void testRotation() {
		LCD.clear(1);
		LCD.clear(2);
		LCD.drawString("Testing Rotation", 0, 1);
		LCD.drawString("Rotating Left 1080", 0, 2);
		
		testDrive.rotate(1080);
		testDrive.stop();
		
		LCD.clear(2);
		LCD.drawString("Rotating Right 1080", 0, 2);
		
		testDrive.rotate(-1080);
		testDrive.stop();
		
	}
	
	
	public void testMotors() {
		LCD.clear();
		drawScreen();
		processInput();
	}
}
