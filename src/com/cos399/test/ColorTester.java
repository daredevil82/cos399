/*	Author: Jason Johns
 * 	Date: 1-16-2013
 * 	Package: COS399_test
 * 	Description: ColorTester execution for Lego Mindstorms MXT robot using leJOS binaries and plugins.
 *  Intended for usage with SensorTester testing suite
 */

package com.cos399.test;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.SensorPort;
import lejos.nxt.addon.ColorHTSensor;


public class ColorTester implements SensorTestInterface {
	private static ColorTester tester = null;
	
	private ColorHTSensor sensor;
	private String[] colorNames = {"RED", "GREEN", "BLUE", "YELLOW", "MAGENTA", 
			"ORANGE", "WHITE", "BLACK", "PINK", "GRAY", "LIGHT GRAY", 
			"DARK GRAY", "CYAN"};
	private int colorComponent;
	private int colorRaw; 
	private int color;
	
	//singleton pattern
	private ColorTester() {}
	
	public static ColorTester getInstance() {
		if (tester == null) {
			tester = new ColorTester();
			tester.sensor = new ColorHTSensor(SensorPort.S1);
		}
		
		return tester;
			
	}
	
	public boolean testSensors(){
		while (!Button.ESCAPE.isDown()) {
			color = sensor.getColorID();
						
			colorComponent = sensor.getRGBComponent(color);
			colorRaw = sensor.getRGBRaw(color);
			
			LCD.drawString("Sensor Reading:", 0, 1);
			LCD.drawString("Name: " +Integer.toString(color), 0, 2);
			LCD.drawString(colorNames[color], 0, 3);			
			LCD.drawString("Raw: " +Integer.toString(colorRaw), 0, 4);
			LCD.drawString("Component: "+ Integer.toString(colorComponent), 0, 5);
			LCD.drawString("Press Enter to quit", 0, 5);
			
		}
		
		return false;
	}

	public static void main(String[] args) {
		ColorTester tester = getInstance();
		tester.testSensors();
	}

}
