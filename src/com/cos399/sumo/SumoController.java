/*	Main class of Sumo project
 * 
 */

package com.cos399.sumo;


import com.cos399.sumo.behaviors.Contact;
import com.cos399.sumo.behaviors.Search;
import com.cos399.sumo.behaviors.Startup;

import com.cos399.sumo.drive.Drive;

import com.cos399.sumo.sensors.SensorInitializer;
import com.cos399.sumo.test.SumoTestController;

import com.cos399.sumo.utils.SumoDataExchange;
import com.cos399.sumo.utils.SumoProperties;

import lejos.nxt.LCD;
import lejos.nxt.Button;

import lejos.robotics.subsumption.*;

import lejos.util.Delay;

public class SumoController {

	private int lineSelection = 2;
	private static SumoController sc = null;
	private static SumoDataExchange de = null;
	
	private void drawChoices() {
		LCD.drawString("Sumo!!", 0, 0);
		LCD.drawString("> 1: Competition", 0, 2);
		LCD.drawString("  2: ActionTest", 0, 3);
		LCD.drawString("  3: Sensor Test", 0, 4);
		LCD.drawString("  4: Motor Test", 0, 5);
		LCD.drawString("  5: Exit", 0, 6);
	}
	
	private void processInput() {
		int button = -1;
		while (true) {
			button = Button.waitForAnyPress();
			
			switch(button) {
				case 1: //enter handler, execute selection option
					switch(lineSelection) {
						case 2:	
							LCD.clear();
							LCD.drawString("Comptetition", 0, 3);
							initializeSumoCompetition();
							Delay.msDelay(1000);
							main(null);						
							break;
							
						case 3:
							SumoTestController.getInstance().testActions();
							Delay.msDelay(1000);
							main(null);
							break;
					
						case 4: //test sensors
							SumoTestController.getInstance().testSensors();
							Delay.msDelay(1000);
							main(null);	
							break;
					
						case 5: //test motors
							SumoTestController.getInstance().testMotors();
							Delay.msDelay(1000);
							main(null);	
							break;
							
						case 6: //exit
							System.exit(2);
							break;
						}
					
					break;
					
				case 2: //left button, move up list
					LCD.clear(0, lineSelection, 1);
					
					if (lineSelection == 2) 
						lineSelection = 6;
					else 
						lineSelection--;
					
					LCD.drawChar('>', 0, lineSelection);
					
					break;
					
				case 4: //right button, move down list
					
					LCD.clear(0, lineSelection, 1);
					if (lineSelection == 6) 
						lineSelection = 2;
					else
						lineSelection++;
					
					LCD.drawChar('>', 0, lineSelection);
					
					break;

			}
		}
	}
	
	private void initializeSumoCompetition() {
		Behavior startupBehavior = new Startup();
		Behavior contactBehavior = new Contact();
		Behavior searchBehavior = new Search();
		Behavior[] behaviorArray = {startupBehavior, contactBehavior, searchBehavior};
		
		SumoProperties sp = SumoProperties.getInstance();
		SensorInitializer.initializeSensors();
		
		LCD.clear();
		
		de.setDrive(Drive.getInstance(sp.getLeftMotorPort(), sp.getRightMotorPort(), 
				sp.getCenterMotorPort(), sp.getMaxRotationSpeed()));
		
		(new Arbitrator(behaviorArray)).start();
	}

	public static void main(String[] args) {
		if (sc == null) {
			sc = new SumoController();
			de = SumoDataExchange.getInstance();
		}
		sc.drawChoices();
		sc.processInput();
	}
	
}
