/*
 * 
 */

package com.cos399.sumo.sensors;

import com.cos399.sumo.utils.SumoDataExchange;

import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;

public class Ultrasonic extends Thread {

	private static Ultrasonic ultrasonic = null;
	private static UltrasonicSensor us = null;
	private static SensorPort port = null;
	private static SumoDataExchange de = null;
	
	private Ultrasonic () { }
	
	public static Ultrasonic getInstance() {
		if (ultrasonic == null) {
			ultrasonic = new Ultrasonic();
			us = new UltrasonicSensor(port);
			us.setMode(2); //set to continuous
			de = SumoDataExchange.getInstance();
		}
		
		return ultrasonic;
	}
	
	public static Ultrasonic getInstance(SensorPort port) {
		Ultrasonic.port = port;
		
		return getInstance();
	}
	
	public void setPort(SensorPort port){
		Ultrasonic.port = port;
	}
	
	public void setContinuous() {
		us.continuous();
		de.setUltrasonicMode(2);
	}
	
	public void setCapture() {
		us.capture();
		de.setUltrasonicMode(3);
	}
	
	public void setPing() {
		us.ping();
		de.setUltrasonicMode(1);
		
	}
	
	public void setOff() {
		us.off();
		de.setUltrasonicMode(0);
	}
	
	@Override
	public void run() {
		while(true) {
			de.setUltrasonicDistance(us.getDistance());
		}
	}
	
}
