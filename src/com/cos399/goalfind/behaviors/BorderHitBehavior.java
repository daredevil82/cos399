/*	Behavior class that activates when border is found.
 * 	Executs a rotation operation to set up for another grid line search
 */

package com.cos399.goalfind.behaviors;

import com.cos399.goalfind.controller.PilotController;
import com.cos399.goalfind.dataexchange.DataExchange;

import lejos.robotics.subsumption.Behavior;

public class BorderHitBehavior implements Behavior {

	private DataExchange de = DataExchange.getInstance();
	private PilotController pc = PilotController.getInstance();
	
	@Override
	public boolean takeControl() {
		return de.getCmd() == 1;
	}

	@Override
	public void action() {
		
		if (pc.getHeadLeft())
			pc.executeRightRotation();
		else
			pc.executeLeftRotation();
		
		de.setCmd(3);
		de.setBorderDetected(false);
		
	}

	@Override
	public void suppress() {
		
		//nothing to suppress here
	}

}
