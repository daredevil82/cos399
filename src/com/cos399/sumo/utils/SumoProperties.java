/*	Hardcoded default values for Sumo Properties, specifically 
 * 	Sensor and Motor ports
 * 
 */

package com.cos399.sumo.utils;

import lejos.nxt.Battery;
import lejos.nxt.SensorPort;
import lejos.nxt.MotorPort;

public class SumoProperties {
	
	private static final MotorPort LEFTMOTORPORT = MotorPort.C;
	private static final MotorPort RIGHTMOTORPORT = MotorPort.A;
	private static final MotorPort CENTERMOTORPORT = MotorPort.B;
	
	private static final SensorPort TOUCHSENSORPORT = SensorPort.S1;
	private static final SensorPort ULTRASONICSENSORPORT = SensorPort.S2;
	private static final SensorPort LIGHTSENSORPORT = SensorPort.S3;
	private static final SensorPort COLORSENSORPORT = SensorPort.S4;
	
	private static int maxRotationSpeed = -1;
	
	
	private static SumoProperties props = null;
	
	private SumoProperties() { }
	
	public static SumoProperties getInstance() {
		if (props == null) {
			props = new SumoProperties();
			setMaxRotationSpeed();
		}
		return props;
	}
	
	public MotorPort getLeftMotorPort() {
		return LEFTMOTORPORT;
	}
	public MotorPort getRightMotorPort() {
		return RIGHTMOTORPORT;
	}
	public MotorPort getCenterMotorPort() {
		return CENTERMOTORPORT;
	}
	public SensorPort getTouchSensorPort() {
		return TOUCHSENSORPORT;
	}
	public SensorPort getUltrasonicSensorPort() {
		return ULTRASONICSENSORPORT;
	}
	public SensorPort getLightSensorPort() {
		return LIGHTSENSORPORT;
	}
	public SensorPort getColorSensorPort() {
		return COLORSENSORPORT;
	}
	
	private static void setMaxRotationSpeed() {
		maxRotationSpeed = (int) Battery.getVoltage() * 1000;
	}
	
	public int getMaxRotationSpeed() {
		return maxRotationSpeed;
	}
	 

}
