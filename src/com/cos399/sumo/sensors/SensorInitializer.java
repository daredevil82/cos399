/*	Initializes all sensors
 * 
 */

package com.cos399.sumo.sensors;

import com.cos399.sumo.utils.SumoProperties;

public class SensorInitializer {

	private static Touch ts = null;
	private static Light ls = null;
	private static Ultrasonic us = null;
	
	private SensorInitializer() { }
	
	public static void initializeSensors() {
		SumoProperties sp = SumoProperties.getInstance();
		
		ts = Touch.getInstance(sp.getTouchSensorPort());
		ls = Light.getInstance(sp.getLightSensorPort(), sp.getColorSensorPort());
		us = Ultrasonic.getInstance(sp.getUltrasonicSensorPort());
		us.setContinuous();
		
		ts.start();
		ls.start();
		us.start();
	}
	
	
	
}
