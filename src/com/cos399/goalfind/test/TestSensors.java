package com.cos399.goalfind.test;

import lejos.nxt.Button;
import lejos.nxt.LCD;

import com.cos399.goalfind.test.sensortest.ColorSensorTester;
import com.cos399.goalfind.test.sensortest.TouchSensorTester;
import com.cos399.goalfind.test.sensortest.UltrasonicSensorTester;
import com.cos399.goalfind.test.SensorTestInterface;
import com.cos399.goalfind.test.TestSensorsDataExchange;

public class TestSensors {
	private static TestSensors testSensors = null;
	
	private TestSensors() { }
	
	//3 is the Y-line value for the default selection
	private int currentSelection = 3;  
	
	public TestSensors getInstance() {
		if (testSensors == null) {
			testSensors = new TestSensors();			
		}
		
		return testSensors;
	}
	
	public void displayMenu() {
		LCD.drawString("Sensor Testing", 0, 0);
		LCD.drawString("> 1: Color", 0, 2);
		LCD.drawString("  2: Touch", 0, 3);
		LCD.drawString("  3: Ultrasonic", 0, 4);
		LCD.drawString("  4: Everything", 0, 5);
		LCD.drawString("  5: Exit", 0, 6);
	}
	
	public void handleSelection() {
		int button = 0;
		SensorTestInterface test = null;
		while (true) {
			button = Button.waitForAnyPress();
			
			if (button == Button.ID_ENTER) {
				LCD.clear();
				switch (currentSelection) {
				
				case 2: //test color sensor
					test = ColorSensorTester.getInstance();
					
					break;
					
				case 3: //test touch sensor
					
					break;
					
				case 4: //test ultrasonic sensor
					
					break;
					
				case 5: //test everything
					
					break;
					
					
				}
			}
		}
	}
	
}
