package com.cos399.goalfind.sensors;

import com.cos399.goalfind.dataexchange.DataExchange;

import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;

public class GoalFindTouchSensor extends Thread{
	
	private static GoalFindTouchSensor goalFindTouch = null;
	private static TouchSensor leftTouch = null, rightTouch = null;
	private static DataExchange de = null;
	
	private GoalFindTouchSensor() {	}
	
	public static GoalFindTouchSensor getInstance(){
		if (goalFindTouch == null ) {
			goalFindTouch = new GoalFindTouchSensor();
			leftTouch = new TouchSensor(SensorPort.S2);
			rightTouch = new TouchSensor(SensorPort.S3);
			de = DataExchange.getInstance();
		}
		
		return goalFindTouch;
	}
	
	@Override
	public void run(){
		while(true){
			if (leftTouch.isPressed()) {
				de.setLeftTouchDetected(true);
				de.setCmd(0);
			} 
			
			if (rightTouch.isPressed()) {
				de.setRightTouchDetected(true);
				de.setCmd(0);
			}
		}
	}

}

