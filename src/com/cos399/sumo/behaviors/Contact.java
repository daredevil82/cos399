/*	Implements power moves during contact to push opposing robot out of ring
 * 
 */

package com.cos399.sumo.behaviors;

import com.cos399.sumo.behaviors.Actions;
import com.cos399.sumo.utils.SumoDataExchange;

import lejos.nxt.LCD;
import lejos.robotics.subsumption.Behavior;

public class Contact implements Behavior{
	
	@Override
	public boolean takeControl() {
		return SumoDataExchange.getInstance().getContact();
	}

	@Override
	public void action() {
		LCD.clear();
		LCD.drawString("Contact Made", 0, 0);
		Actions.getInstance().attack();
	}

	@Override
	public void suppress() {

	}
}
