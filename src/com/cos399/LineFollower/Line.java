/*	Author: 	Jason Johns, Sallie Hutchinson
 * 	Project: 	COS 399 Line Follower 1
 * 	Date:		1-23-2013
 * 	Description:
 *		This class executes the requirements for the Line Follower 1 project
 *		for COS 399 Robotics using the Lejos API. Currently, only the 
 *		DriveForward and OffLine behaviors work as advertised.  FollowLine
 *		and FindLine are broken functionality in an attempt to directly control
 *		the motors themselves to increase speed.
 *
 *		Current project is broken, despite duplicating the functionality of LineFollower.java.
 *		Unknown reasons why.
 * 
 */

package com.cos399.LineFollower;

import com.cos399.LineFollower.Robot;

import lejos.nxt.*;
import lejos.robotics.subsumption.*;



public class Line {

	public Arbitrator lineFollower;
	public String behaviorType;
	public static int curvature = 0;
	
	//type == true, execute with DriveForward and OffLine behaviors.
	//type == false, execute with FindLine and FollowLine behaviors
	public Line(Robot robot, boolean type){
			Behavior onLine = new DriveForward(robot);
			Behavior offLine = new OffLine(robot);
			Behavior[] behaviors = {onLine, offLine};
			behaviorType = "Pilot.rotate";
			lineFollower = new Arbitrator(behaviors, true);
		
	}
	
	//execution function for line follower
	private void execute(){
		LCD.clearDisplay();
		LCD.drawString("Line Follower", 0, 0);
		LCD.drawString("Type: " + behaviorType, 0, 1);
		LCD.drawString("Press any button", 0, 2);
		LCD.drawString("to begin", 0, 3);
		
		Button.waitForAnyPress();
		lineFollower.start();
	}
	
	
	/*	Main function for line follower program.  User presses left or right buttons
	 * 	to select type of behaviors to use.  The appropriate constructor args
	 * 	are passed to the constructor and robot begins executing.
	 * 
	 */
	
	public static void main(String[] args) throws Exception{
        
		Robot robot = new Robot();
		LCD.drawString("Line Follower", 0, 0);
		LCD.drawString("Select Type: ", 0, 1);
		LCD.drawString("L button: Pilot", 0, 2);
		LCD.drawString("R button: Direct", 0, 3);

		Line line = new Line(robot, true);
		line.execute();	

	}

}
