/*	Implements a search using ultrasonic sensor when contact has been lost
 * 
 */

package com.cos399.sumo.behaviors;

import com.cos399.sumo.behaviors.Actions;
import com.cos399.sumo.utils.SumoDataExchange;

import lejos.nxt.LCD;
import lejos.robotics.subsumption.Behavior;

public class Search implements Behavior {

	@Override
	public boolean takeControl() {
		return !SumoDataExchange.getInstance().getIsStart() 
				&& SumoDataExchange.getInstance().getBoundaryDetected();
	}
	
	@Override
	public void action() {
		LCD.clear();
		LCD.drawString("Search Action", 0, 0);
		
		Actions.getInstance().backOffAndRotate();
		Actions.getInstance().forward();
		
	}

	@Override
	public void suppress() {

	}

}
