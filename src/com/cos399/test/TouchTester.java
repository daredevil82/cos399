/*	Testing object for touch sensors.  Assumes that robot has 2 touch sensors
 * 	mapped as Sensor-Port (Right, S2) and (Left, S3)
 * 
 */

package com.cos399.test;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;

public class TouchTester implements SensorTestInterface {
	
	private static TouchTester tester = null;
	
	private TouchSensor leftSensor;
	private TouchSensor rightSensor;
	
	private TouchTester(){ }
	
	public static TouchTester getInstance(){
		if (tester == null) {
			tester = new TouchTester();
			tester.leftSensor = new TouchSensor(SensorPort.S2);
			tester.rightSensor = new TouchSensor(SensorPort.S3);
			
			LCD.clear();
			LCD.drawString("Touch Testing" , 0, 1);
		}
		return tester;
	}
	
	public boolean testSensors(){
		
		while (!Button.ESCAPE.isDown()){
			if (leftSensor.isPressed()) {
				LCD.clear(3);
				LCD.drawString("Left Sensor Hit" , 0, 3);
			} 
			
			if (rightSensor.isPressed()) {
				LCD.clear(3);
				LCD.drawString("Right Sensor Hit", 0, 3);
			}
		}
		
		return false;
	}
	
	public static void main(String[] args) {
		
		TouchTester tester = TouchTester.getInstance();
		tester.testSensors();
		
	}

}
