/*	Authors: 	Jason Johns, Sallie Hutchinson
 * 	Date:		1-30-2013
 * 	Project:	COS 399 Line Follower 1
 * 	Description:
 * 		This class implements the Behavior interface to on-line drive functionality.
 * 		This class is executed upon a RIGHT button press at start of program.  
 * 		
 * 
 */

package com.cos399.LineFollower;

import com.cos399.LineFollower.Robot;

import lejos.nxt.LCD;
import lejos.robotics.subsumption.Behavior;

public class FollowLine implements Behavior {

	private boolean suppressed;
	private Robot robot;
	
	public FollowLine(Robot robot){
		this.robot = robot;
		this.suppressed = false;
	}
	
	@Override
	public boolean takeControl() {
		LCD.drawString(Integer.toString(robot.light.readNormalizedValue()), 0, 1);
		return robot.isElectricTapeLine();
	}

	@Override
	public void action() {
		suppressed = false;
		//robot.pilot.steer(LineFollower.curvature);
		LCD.drawString(Integer.toString(robot.light.readNormalizedValue()), 0, 1);
		while (!suppressed && robot.isElectricTapeLine());
		
	}

	@Override
	public void suppress() {
		suppressed = true;
		
	}
	

}
