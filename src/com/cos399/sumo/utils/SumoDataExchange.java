package com.cos399.sumo.utils;

import com.cos399.sumo.drive.Drive;

public class SumoDataExchange {

	private static SumoDataExchange de = null;
	
	
	private boolean boundaryDetected = false, search = false, isStart = true;
	private boolean initialContact = false, contact = false;
	private int ultrasonicDistance = -1, usMode = -1;
	private String boundaryDetectedLightSensor = "";
	
	private Drive drive = null;
	
	
	private SumoDataExchange() { }
	
	public static SumoDataExchange getInstance() {
		if (de == null)  {
			de = new SumoDataExchange();
		}
		return de;
	}
	
	public boolean getBoundaryDetected() {
		return boundaryDetected;
	}
	
	public void setBoundaryDetected(boolean status) {
		boundaryDetected = status;
	}
	
	public void setBoundaryDetectedLightSensor(String sensor) {
		boundaryDetectedLightSensor = sensor;
	}
	
	public String getBoundaryDetectedLightSensor() {
		return boundaryDetectedLightSensor;
	}
	
	public void setInitialContact(boolean status) {
		this.initialContact = status;
	}
	
	public boolean getInitialContact() { 
		return initialContact;
	}
	
	public void setContact(boolean status) {
		contact = status;
	}
	
	public boolean getContact() {
		return contact;
	}
	
	public void setUltrasonicDistance(int distance) {
		ultrasonicDistance = distance;
	}
	
	public int getUltrasonicDistance() {
		return ultrasonicDistance;
	}
 	
	public int getUltrasonicMode(){
		return usMode;
	}
	
	public void setUltrasonicMode(int mode) {
		usMode = mode;
	}
	
	public boolean getIsStart() {
		return isStart;
	}
	
	public boolean getSearch() {
		return search;
	}
	
	public void setSearch(boolean status) {
		this.search = status;
	}
	
	public void setIsStart(boolean status) {
		this.isStart = status;
	}
	
	public void setDrive(Drive drive) {
		this.drive = drive;
	}
	
	public Drive getDrive(){
		return drive;
	}
	
	
	
}
