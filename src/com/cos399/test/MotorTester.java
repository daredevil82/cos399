/*	Testing class to analyze motor functions.  Results are used to implement
 * 	an xy coordinate grid search
 * 
 * 	Standalone testing class
 * 
 */

package com.cos399.test;

import java.io.IOException;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.Motor;

import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.RegulatedMotor;

import lejos.util.Delay;
import lejos.util.PilotProps;
import lejos.util.Timer;



public class MotorTester implements SensorTestInterface {
	
	private static MotorTester tester = null;
	private static Timer timer = null;
	private static TimerTester timeOut = null;
	
	private RegulatedMotor leftMotor, rightMotor;
	private DifferentialPilot pilot;
	private int leftTach = 0, rightTach = 0, travelSpeed = 50;
	
	private MotorTester() {	}
	
	public static MotorTester getInstance() {
		if (tester == null) {
			tester = new MotorTester();
			timeOut = new TimerTester();
			timer = new Timer(500, timeOut);
			
			PilotProps pp = new PilotProps();
	        try {
				pp.loadPersistentValues();
			} catch (IOException e) {
				e.printStackTrace();
			}

	        //initialize and set pilot properties
	        float wheelDiameter = Float.parseFloat(pp.getProperty(
	        		PilotProps.KEY_WHEELDIAMETER, "2.25"));
	        
	        float trackWidth = Float.parseFloat(pp.getProperty(
	        		PilotProps.KEY_TRACKWIDTH, "6.6875"));
	        
	        tester.leftMotor = PilotProps.getMotor(pp.getProperty(
	        		PilotProps.KEY_LEFTMOTOR, "A"));
	        
	        tester.rightMotor = PilotProps.getMotor(pp.getProperty(
	        		PilotProps.KEY_RIGHTMOTOR, "C"));
	        
	        boolean reverse = Boolean.parseBoolean(pp.getProperty(
	        		PilotProps.KEY_REVERSE, "false"));
	        
	        tester.pilot = new DifferentialPilot(wheelDiameter, trackWidth, 
	        		tester.leftMotor, tester.rightMotor, reverse);
		}
		
		return tester;
	}
	
	public boolean testSensors(){
		return testMotors();
	}
	
	private boolean testMotors() {
		
		LCD.drawString("Speed: " + travelSpeed, 0, 0);
		LCD.drawString("L/R Diff: ", 0, 6);
		
		pilot.setRotateSpeed(720);
		timer.start();
		pilot.forward();
		
		while (!Button.ESCAPE.isDown()) {
			leftTach = Motor.A.getTachoCount();
			rightTach = Motor.C.getTachoCount();
		}
		
		pilot.stop();
		Delay.msDelay(5000);
		
		return false;
	}
	
	public void displayText(){
		LCD.drawString("Motor Tester", 0, 0);
		LCD.drawString("Press Escape\nto exit", 0, 1);
		
	}

	public int getLeftTach() {
		return leftTach;
	}

	public void setLeftTach(int leftTach) {
		this.leftTach = leftTach;
	}

	public int getRightTach() {
		return rightTach;
	}

	public void setRightTach(int rightTach) {
		this.rightTach = rightTach;
	}

	public static void main(String[] args) {
		
		MotorTester.tester = MotorTester.getInstance();
		tester.testMotors();
		
	}

}
