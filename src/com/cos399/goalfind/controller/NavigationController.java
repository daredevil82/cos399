/*	Implements navigation and mapping functionality for goal find project
 * 
 */

package com.cos399.goalfind.controller;


import java.util.ArrayList;

import com.cos399.goalfind.dataexchange.DataExchange;


public class NavigationController extends Thread{
	
	private static NavigationController nav = null;
	private static DataExchange de = null;
	
	private boolean isStart = true, findCorner = true, executeSearch = false;
	private static final int STARTHEADING = 0;
	private int XPos, YPos;
	
	private NavigationController() { }
	
	public static NavigationController getInstance() {
		if (nav == null) {
			nav = new NavigationController();
			de = DataExchange.getInstance();
			nav.XPos = 0;
			nav.YPos = 0;
		}
		return nav;
	}
	
	//record an obstacle location
	public void recordObstacle(int XTach, int YTach, int heading) {
		
	}
	
	
	public void setXPos(int value) {
		XPos = value;
	}
	
	public int getXPos() {
		return XPos;
	}
	
	public void setYPos(int value) {
		YPos = value;
	}
	
	public int getYPos() {
		return YPos;
	}


	
	

	
	
	

}
