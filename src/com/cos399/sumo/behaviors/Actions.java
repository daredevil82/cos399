/*	Defining actions of sumo moves.
 * 
 */

package com.cos399.sumo.behaviors;

import com.cos399.sumo.drive.Drive;
import com.cos399.sumo.utils.SumoDataExchange;

public class Actions {

	private static Actions actions = null;
	private static Drive drive = null;
	private static SumoDataExchange de = null;
	
	private Actions() { }
	
	public static Actions getInstance() {
		if (actions == null) {
			actions = new Actions();
			de = SumoDataExchange.getInstance();
			drive = de.getDrive();
		}
		return actions;
	}

	
	public void attack() {
		
		if (de.getSearch())
			de.setSearch(false);
		
		drive.forward();

	}
	
	public void backOffAndRotate() {
		drive.stop();
		
		if (de.getBoundaryDetectedLightSensor() == "color") {
			drive.backward(15);
			drive.rotate(45);
		} else {
			drive.forward(15);
			drive.rotate(-45);
		}
		
	}
	
	public void forward() {
		drive.forward();
	}
	
	
	
}
