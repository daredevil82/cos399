package com.cos399.sumo.test;

import com.cos399.sumo.SumoController;
import com.cos399.sumo.behaviors.Actions;
import com.cos399.sumo.drive.Drive;
import com.cos399.sumo.utils.SumoDataExchange;
import com.cos399.sumo.utils.SumoProperties;

import lejos.nxt.Button;
import lejos.nxt.LCD;

public class ActionTest {

	private static ActionTest at = null;
	private static SumoDataExchange de = null;
	private static int lineSelection = 2;
	
	private ActionTest() { }
	
	public static ActionTest getInstance() {
		if (at == null) {
			at = new ActionTest();
			de = SumoDataExchange.getInstance();
			SumoProperties sp = SumoProperties.getInstance(); 
			de.setDrive(Drive.getInstance(sp.getLeftMotorPort(), sp.getRightMotorPort(), 
					sp.getCenterMotorPort(), sp.getMaxRotationSpeed()));
		}
		
		return at;
	}
	
	private void drawScreen() {
		LCD.clear();
		LCD.drawString("ActionTests", 0, 0);
		LCD.drawString("> 1: Attack", 0, 2);
		LCD.drawString("  2: Retreat", 0, 3);
		LCD.drawString("  3: Backup/Rotate", 0, 4);
		LCD.drawString("  4: Exit", 0, 5);
	}
	
	private void processInput() {
		int button = -1;
		while (!Button.ESCAPE.isDown()) {
			button = Button.waitForAnyPress();
			
			switch(button) {
				case 1: //enter
					switch(lineSelection) {
						case 2:
							Actions.getInstance().attack();
							testActions();
							break;
						case 3:
							testActions();
							break;
						case 4:
							Actions.getInstance().backOffAndRotate();
							testActions();
							break;
						case 5:
							SumoController.main(null);
					}
					break;
				case 2: //left button, move up list
					LCD.clear(0, lineSelection, 1);
					
					if (lineSelection == 2) 
						lineSelection = 5;
					else
						lineSelection--;
					
					LCD.drawChar('>', 0, lineSelection);
					
					break;
				case 4: //right button, move down list
					LCD.clear(0, lineSelection, 1);
					
					if(lineSelection == 5) 
						lineSelection = 2;
					else
						lineSelection++;
					
					LCD.drawChar('>', 0, lineSelection);
					
					break;
				case 8: //escape, return to main menu
					SumoController.main(null);
					break;
			}
		}
	}
	
	public void testActions() {
		LCD.clear();
		drawScreen();
		processInput();
	}
	
}
