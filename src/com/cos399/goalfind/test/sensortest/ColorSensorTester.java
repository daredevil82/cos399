package com.cos399.goalfind.test.sensortest;

import com.cos399.goalfind.test.SensorTestInterface;


import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.SensorPort;
import lejos.nxt.addon.ColorHTSensor;



public class ColorSensorTester extends Thread implements SensorTestInterface {

	private static ColorSensorTester colorTester = null;
	private static ColorHTSensor colorSensor = null;
	
	private String[] colorNames = {"RED", "GREEN", "BLUE", "YELLOW", "MAGENTA", 
			"ORANGE", "WHITE", "BLACK", "PINK", "GRAY", "LIGHT GRAY", 
			"DARK GRAY", "CYAN"};
	
	private ColorSensorTester() {}
	
	public static ColorSensorTester getInstance() {
		if (colorTester == null) {
			colorTester = new ColorSensorTester();
			colorSensor = new ColorHTSensor(SensorPort.S1);
		}
		
		return colorTester;
	}
	
	@Override
	public boolean testSensors() {
		
		return false;
	}
	
	@Override
	public void run() {
		testSensors();
	}

}
