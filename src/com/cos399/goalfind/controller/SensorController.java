/*	Instanciates and starts the sensor threads.  Should be called only once
 * 	in the program lifetime.
 * 
 */

package com.cos399.goalfind.controller;

import com.cos399.goalfind.sensors.GoalFindColorSensor;
import com.cos399.goalfind.sensors.GoalFindTouchSensor;
import com.cos399.goalfind.sensors.GoalFindUltrasonicSensor;

public class SensorController {

	private static SensorController se = null;
	private static GoalFindColorSensor color = null;
	private static GoalFindTouchSensor touch = null;
	private static GoalFindUltrasonicSensor ultrasonic = null;
	
	private SensorController() { }
	
	public static SensorController getInstance() {
		if (se == null) {
			se = new SensorController();
			color = GoalFindColorSensor.getInstance();
			touch = GoalFindTouchSensor.getInstance();
			ultrasonic = GoalFindUltrasonicSensor.getInstance();
			
			se.startThreads();
		}
		
		return se;
	}
	
	private void startThreads() {
		color.start();
		touch.start();
		ultrasonic.start();
	}
}
