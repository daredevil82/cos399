package com.cos399.test;

import lejos.nxt.LCD;


public class TimerTester implements lejos.util.TimerListener{

	MotorTester tester = MotorTester.getInstance();
	int leftTach, rightTach;
	double differential;
	
	@Override
	public void timedOut() {
		leftTach = tester.getLeftTach();
		rightTach = tester.getRightTach();
		
				
		LCD.clear(2);
		LCD.clear(3);
		LCD.clear(7);
		LCD.drawString("LTach: " + leftTach, 0, 2);
		LCD.drawString("RTach: " + tester.getRightTach(), 0, 3);
		LCD.drawString(Double.toString(leftTach - rightTach), 0, 7);
		
	}

}
