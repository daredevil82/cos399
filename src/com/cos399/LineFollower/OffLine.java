/*	Authors: 	Jason Johns, Sallie Hutchinson
 * 	Date:		1-30-2013
 * 	Project:	COS 399 Line Follower 1
 * 	Description:
 * 		This class implements the Behavior interface to achieve offline search functionality.
 * 		The robot, once off-line, has to have a way to search to get back on track, and our
 * 		main functionality is in the action() method.  This class is executed upon a LEFT
 * 		button press at start of program.
 * 		
 * 
 */

package com.cos399.LineFollower;

import com.cos399.LineFollower.Robot;

import lejos.nxt.LCD;
import lejos.robotics.subsumption.Behavior;

public class OffLine implements Behavior {

	private boolean suppressed;
	private Robot robot;
	
	public OffLine(Robot robot){
		this.suppressed  = false;
		this.robot = robot;
	}
	
	@Override
	public boolean takeControl() {
		return robot.light.readValue() > robot.defaultThreshold;
	}

	@Override
	public void action() {
		int sweepAngle = 10;
		boolean sweep = false;
		
		if (robot.light.readValue() <= robot.defaultThreshold){
			sweep = true;
			sweepAngle = Math.abs(sweepAngle);
		}
		
		while (!suppressed){
			robot.pilot.rotate(sweepAngle, true);
			
			while (!suppressed && robot.pilot.isMoving())
				Thread.yield();
			
			if (robot.light.readValue() <= robot.defaultThreshold){
				sweep = false;
				sweepAngle = Math.abs(sweepAngle);
			}
			
			if (sweep)
				sweepAngle *= 2;
			else
				sweepAngle *= -2;
			
			LCD.clearDisplay();
			LCD.drawString("SweepAngle: " + sweepAngle, 0, 1);
			LCD.drawString("Sensor: " + robot.light.readValue(), 0, 2);
		}
		
		robot.pilot.stop();
		suppressed = false;

	}

	@Override
	public void suppress() {
		suppressed = true;
		while (suppressed)
			Thread.yield();

	}

}
