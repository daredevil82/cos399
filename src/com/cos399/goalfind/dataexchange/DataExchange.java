/*	Mediates data exchange between different sensor threads
 * 	Is monitored by the Pilot and Navigation classes
 */

package com.cos399.goalfind.dataexchange;

public class DataExchange {
	
	private boolean obstacleDetected;
	private boolean ultrasonicDetected;
	private boolean leftTouchDetected, rightTouchDetected, borderDetected;
	private int colorDetected;
	private String colorDetectedName;
	
	/*CMD values:
	 * 	0	Obstacle Detected via either touch or ultrasonic
	 * 	1	Line boundary detected
	 * 	2	Goal Found
	 * 	3	All Clear
	*/
	private int cmd = 3;
	private int angleDetected = 0;
	
	private static DataExchange de = null;
	
	private DataExchange() { }
	
	//get the instanciated singleton instance of this object
	public static DataExchange getInstance(){
		if (de == null){
			de = new DataExchange();
			de.obstacleDetected = false;
			de.ultrasonicDetected = false;
			de.leftTouchDetected = false;
			de.rightTouchDetected = false;
			de.borderDetected = false;
			de.colorDetected = -1;
			de.colorDetectedName = "";
		}
		
		return de;
	}
	
	
	//getters and setters for all data exchange variables
	public boolean getUltrasonicDetected(){
		return ultrasonicDetected;
	}
	
	public void setUltrasonicDetected(boolean status){
		this.ultrasonicDetected = status;
	}
	
	public boolean getLeftTouchDetected(){
		return leftTouchDetected;
	}
	
	public void setLeftTouchDetected(boolean status){
		leftTouchDetected = status;
	}
	
	public boolean getRightTouchDetected(){
		return rightTouchDetected;
	}
	
	public void setRightTouchDetected(boolean status){
		rightTouchDetected = status;
	}
	
	public void setObstacleDetected(boolean status){
		obstacleDetected = status;
	}
	
	public boolean getObstacleDetected(){
		return obstacleDetected;
	}

	public int getCmd(){
		return cmd;
	}
	
	public void setCmd(int cmd){
		this.cmd = cmd;
	}
	
	public void setAngleDetected(int angle){
		angleDetected = angle;
	}
	
	public int getAngleDetected(){
		return angleDetected;
	}
	
	public void setColorDetected(int colorValue){
		colorDetected = colorValue;
	}
	
	public int getColorDetected(){
		return colorDetected;
	}
	
	public String getColorName(){
		return colorDetectedName;
	}

	public void setColorName(String color) {
		colorDetectedName = color;
	}
	
	public boolean getBorderDetected(){
		return borderDetected;
	}
	
	public void setBorderDetected(boolean status){
		borderDetected = status;
	}
	
}
