/*	Authors: 	Jason Johns, Sallie Hutchinson
 * 	Date:		1-30-2013
 * 	Project:	COS 399 Line Follower 1
 * 	Description:
 * 		This class implements the Behavior interface to achieve driveForward on-line functionality.
 * 		This class is executed upon a LEFT button press at start of program.
 * 		
 * 
 */

package com.cos399.LineFollower;

import com.cos399.LineFollower.Robot;

import lejos.nxt.LCD;
import lejos.robotics.subsumption.Behavior;

public class DriveForward implements Behavior {
	private Robot robot;
	
	public DriveForward(Robot robot){
		this.robot = robot;
	}
	
	@Override
	public boolean takeControl() {
		return robot.light.readValue() <= robot.defaultThreshold;
	}

	@Override
	public void action() {
		while (robot.light.readValue() <= robot.defaultThreshold){
			Thread.yield();
			robot.pilot.forward();
			LCD.clear();
			LCD.drawString("Sensor: " + robot.light.readValue(), 0, 1);
			
		}

	}

	@Override
	public void suppress() {
		robot.pilot.stop();

	}

}
