package com.cos399.goalfind.sensors;

import com.cos399.goalfind.dataexchange.DataExchange;

import lejos.nxt.addon.ColorHTSensor;
import lejos.nxt.SensorPort;

public class GoalFindColorSensor extends Thread{

	private static GoalFindColorSensor goalFindColor = null;
	private static ColorHTSensor colorSensor = null;
	private static DataExchange de = null;
	
	private static int borderColor = 7, currentColor, goalColor;
	
	private String colorNames[] = {"RED", "GREEN", "BLUE", "YELLOW", "MAGENTA", 
			"ORANGE", "WHITE", "BLACK", "PINK", "GRAY", "LIGHT GRAY", "DARK GRAY", 
			"CYAN"};
	
	private GoalFindColorSensor() { }
	
	public static GoalFindColorSensor getInstance(){
		if (goalFindColor == null) {
			goalFindColor = new GoalFindColorSensor();
			de = DataExchange.getInstance();
			colorSensor = new ColorHTSensor(SensorPort.S1);
		}
		
		return goalFindColor;
	}
	
	
	@Override
	public void run(){
		while (true) {
			currentColor = colorSensor.getColorID();
			de.setColorDetected(currentColor);
			de.setColorName(colorNames[currentColor]);
			
			//boundary found, reset cmd value
			if (currentColor == borderColor){
				de.setBorderDetected(true);
				de.setCmd(1);
			} 
			
			//goal found
			else if (currentColor == goalColor) {
				de.setCmd(2);
			}
			
			//all clear
			else {
				de.setBorderDetected(false);
				de.setCmd(3);
			}
		}
	}
	
	
	
	
	
}
