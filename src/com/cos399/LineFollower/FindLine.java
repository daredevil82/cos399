/*	Authors: 	Jason Johns, Sallie Hutchinson
 * 	Date:		1-30-2013
 * 	Project:	COS 399 Line Follower 1
 * 	Description:
 * 		This class implements the Behavior interface to achieve offline search functionality.
 * 		The robot, once off-line, has to have a way to search to get back on track, and our
 * 		main functionality is in the action() method.  This class is executed upon a RIGHT
 * 		button press at start of program.
 * 		
 * 
 */

package com.cos399.LineFollower;

import com.cos399.LineFollower.Robot;

import lejos.nxt.LCD;
import lejos.robotics.subsumption.Behavior;

public class FindLine implements Behavior {

	private Robot robot;
	private boolean suppressed;
	
	public FindLine(Robot robot){
		this.robot = robot;
		this.suppressed = false;
		
	}
	
	@Override
	public boolean takeControl() {
		LCD.drawString(Integer.toString(robot.light.readNormalizedValue()), 0, 1);
		return !robot.isElectricTapeLine();
	}

	@Override
	public void action() {
		int sweep = 10;
		int sign = -1;
		while (!suppressed && robot.isElectricTapeLine()){
			robot.pilot.rotate(sign * sweep, true);
			LCD.drawString(Integer.toString(robot.light.readNormalizedValue()), 0, 1);
			while (!suppressed && !robot.isElectricTapeLine() && robot.pilot.isMoving());
			robot.pilot.stop();
			
			sweep *= 2;
			sign *= -1;
		}
		
		Line.curvature += (20 * sign);
		
	}

	@Override
	public void suppress() {
		suppressed = true;
		
	}
	

}
