package com.cos399.LineFollower;

import lejos.nxt.Motor;
import lejos.robotics.RegulatedMotor;
import lejos.nxt.LightSensor;
import lejos.nxt.SensorPort;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.navigation.RotateMoveController;



public class Robot {

	public LightSensor light;
	public RegulatedMotor left, right;
	
	public RotateMoveController pilot;
	public int defaultSpeed = 180;
	public int defaultPower = 80;
	public int defaultThreshold = 45;
	
	
	//Constructor takes in an int.  0 returns a DifferentialPilot object, 1 returns a 
	//RotateMoveController object
	public Robot(){
		left = Motor.C;
		right = Motor.A;
		light = new LightSensor(SensorPort.S1);
		pilot = new DifferentialPilot(2.2f, 6.875f, left, right);
		left.setSpeed(defaultSpeed);
		right.setSpeed(defaultSpeed);
		
	}
	
	public boolean isLine(){
		return light.readValue() < defaultThreshold;
	}
	
	public boolean isElectricTapeLine(){
		return light.readNormalizedValue() <= 375;
	}

}
