/*	Monitors and updates data exchange object on a touch or bump
 * 
 */

package com.cos399.sumo.sensors;

import com.cos399.sumo.utils.SumoDataExchange;

import lejos.nxt.LCD;
import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;

public class Touch extends Thread{
	
	private static Touch touch = null;
	private static TouchSensor ts = null;
	private static SensorPort port = null;
	private static SumoDataExchange de = null;
	private Touch() { }
	
	public static Touch getInstance(SensorPort port) {
		if (touch == null) {
			touch = new Touch();
			ts = new TouchSensor(port);
			de = SumoDataExchange.getInstance();
		}
		
		return touch;
	}
	
	public static Touch getInstance() {
		if (touch == null && port == null) {
			throw new UnsupportedOperationException("SensorPort not bound");
		} else if (touch == null && port != null) {
			touch = new Touch();
			ts = new TouchSensor(port);
		}
		
		return touch;
	}
	
	public static void setSensorPort(SensorPort port) {
		Touch.port = port;
	}
	
	@Override
	public void run() {
		while(true) {
			if (ts.isPressed()) {
				if (!de.getInitialContact())
					de.setInitialContact(true);
				
				de.setContact(true);
				de.setSearch(false);
				
			} else {
				de.setContact(false);
				de.setSearch(true);
			}
			
			LCD.drawString("Contact" + de.getContact(),  0, 1);
		}
	}

}
