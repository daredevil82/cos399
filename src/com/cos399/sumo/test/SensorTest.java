/*	Sensor testing class utility.  Monitors data exchange object and updates
 * 	when changes occur.
 * 
 */

package com.cos399.sumo.test;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.comm.RConsole;

import com.cos399.sumo.SumoController;
import com.cos399.sumo.sensors.SensorInitializer;
import com.cos399.sumo.utils.SumoDataExchange;

public class SensorTest {
	
	private static SensorTest st = null;
	private static SumoDataExchange de = null;
	
	private SensorTest() { }
	
	public static SensorTest getInstance() {
		if (st == null) {
			st = new SensorTest();
			de = SumoDataExchange.getInstance();
		}
		
		return st;
	}
	
	public void testSensors() {
		SensorInitializer.initializeSensors();
		de.setUltrasonicMode(2);
		
		boolean contact = de.getContact();
		boolean boundaryDetected = de.getBoundaryDetected();
		int ultrasonicDistance = de.getUltrasonicDistance();
		RConsole.openAny(1000);
		
		if (!RConsole.isOpen()){
			LCD.clear();
			LCD.drawString("Sensor Tests", 0, 0);
			
		}
		
		while (!Button.ESCAPE.isDown()) {
			if (de.getContact() != contact)  {
				contact = de.getContact();
				
				if (RConsole.isOpen())
					RConsole.println("Touch sensor tripped");
				else if (contact == true){
					LCD.clear(2);
					LCD.drawString("Touch Hit", 0, 2);
				} else {
					LCD.clear(2);
					LCD.drawString("Touch None", 0, 2);
				}
			}
			
			if (de.getBoundaryDetected() != boundaryDetected) {
				boundaryDetected = de.getBoundaryDetected();
				
				if (RConsole.isOpen())
					RConsole.println("Boundary detected by " + de.getBoundaryDetectedLightSensor());
				else{ 
					LCD.drawString("Boundary Detected", 0, 3);
					LCD.clear(4);
					LCD.drawString(de.getBoundaryDetectedLightSensor(), 0, 4);
				}
			}
			
			if (de.getUltrasonicDistance() != ultrasonicDistance) {
				ultrasonicDistance = de.getUltrasonicDistance();
				
				if (RConsole.isOpen())
					RConsole.println("Ultrasonic Distance changed: " + de.getUltrasonicDistance());
				
				else {
					LCD.clear(5);
					LCD.drawString("Sonar: " + de.getUltrasonicDistance(), 0, 5);
				}
				
			}
		}
		
		RConsole.close();
		LCD.clear();
		SumoController.main(null);
	}
	
}
