/*	Sensor testing utility to get experience with ultrasonic and touch sensors.
 * 	Used for a robot with the following mappings:
 * 
 * 		S1:	ColorHTSensor
 * 		S2: Lego Touch, Right
 * 		S3:	Lego Touch, Left
 * 		S4:	Lego Ultrasonic, connected to Motor.B for horizontal sweeping
 * 
 */

package com.cos399.test;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.comm.RConsole;



public class SensorTester {
	
	private int currentSelection = 3;
	
	public SensorTester() { }

	public void displayText(){
		LCD.drawString("Sensor Testing \nUtility", 0, 0);
		LCD.drawString(">1: GoalFindColorSensor", 0, 3);
		LCD.drawString(" 2: TouchSensor", 0, 4);
		LCD.drawString(" 3: Ultrasonic", 0, 5);
		LCD.drawString(" 4: Exit", 0, 6);
	}
	
	
	/*	Handles the button action by redrawing > selector indicator based on
	 * 	left-right button presses.  Pressing enter executes the selected test.
	 * 	For reference, button IDs are
	 * 		Enter	1	ID_ENTER
	 * 		Left 	2	ID_LEFT
	 * 		Right	4	ID_RIGHT
	 * 		Escape	8	ID_ESCAPE
	 */
	private void handleSelection(){
		
		int button = 0;
		boolean loopContinue = true;
		while (loopContinue){
			button = Button.waitForAnyPress();
			//RConsole.println("Button ID Pressed: " + button);
			if (button == Button.ID_ENTER){
				LCD.clear();
				SensorTestInterface tester;
				switch (currentSelection) {
				
					case 3:
						tester = ColorTester.getInstance();
						loopContinue = tester.testSensors();
						break;
					case 4:
						tester = TouchTester.getInstance();
						loopContinue = tester.testSensors();
						break;
					case 5: 
						tester = UltrasonicTester.getInstance();
						loopContinue = tester.testSensors();
						break;
					case 6:
						System.exit(0);
						break;
				}
			}

			if (button == Button.ID_RIGHT)
				updateSelector("down");
			if (button == Button.ID_ESCAPE)
				System.exit(0);
			
		}
		
		displayText();
		handleSelection();
		
	}
	
	/*	Makes sure that the selector is within the range 3:6 and update selection
	 * 
	 */
	private void updateSelector(String direction){
		
		int oldSelection = currentSelection;

		//RConsole.println("Selector: " + direction);
		
		//Update selection variable based on button input
		if (currentSelection + 1 == 7) {
			currentSelection = 3;
			//RConsole.println("Reset to 3");
		} else {
			currentSelection += 1;
			//RConsole.println("Addition\n");
		}
		
		
		//Clear the first character of the display at the old line
		//and add a > char to the new selection line
		LCD.clear(0, oldSelection, 1);
		LCD.drawChar('>', 0, currentSelection);
		
		/*
		RConsole.println("Old selection: " + oldSelection + 
				"\nCurrent Selection: " + currentSelection);
		*/
	}
	
	public static void main(String[] args) {
		SensorTester tester = new SensorTester();
		tester.displayText();
		//RConsole.open();
		tester.handleSelection();
	}

}
