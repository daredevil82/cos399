/*	Self-contained singleton object to hold the Pilot properties as well as 
 * 	speed, travel, rotation and position variables.
 * 
 * 	Implements several movement actions.  Assumes that the grid search area
 * 	is a XY coordinate graph with the start position as (0, 0), and zero degrees
 * 	heading is parallel to the Xaxis from -infinity to infinity (ie heading right).
 */

package com.cos399.goalfind.controller;

import com.cos399.goalfind.controller.NavigationController;
import com.cos399.goalfind.dataexchange.DataExchange;

import java.io.IOException;

import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.RegulatedMotor;
import lejos.util.PilotProps;

public class PilotController{
	
	private static DifferentialPilot pilot;
	private static RegulatedMotor leftMotor, rightMotor;
	
	private static int wheelSpeed = 50, travelDistance = 4;
	private int heading, XTach, YTach;
	private static boolean goalNotFound, headLeft;
	
	private static PilotController pc = null;
	private static NavigationController nc = null;
	private static DataExchange de = null;
	
	private PilotController() { 	}
	
	public static PilotController getInstance() {
		if (pc == null) {
			pc = new PilotController();
			nc = NavigationController.getInstance();
			de = DataExchange.getInstance();
			goalNotFound = false;
			headLeft = true;
			
			
			pc.initializeGoalFindPilot();
		}
		
		return pc;
	}
	
	private void initializeGoalFindPilot(){
		PilotProps pp = new PilotProps();
        try {
			pp.loadPersistentValues();
		} catch (IOException e) {
			e.printStackTrace();
		}

        //initialize and set pilot properties
        float wheelDiameter = Float.parseFloat(pp.getProperty(
        		PilotProps.KEY_WHEELDIAMETER, "2.25"));
        
        float trackWidth = Float.parseFloat(pp.getProperty(
        		PilotProps.KEY_TRACKWIDTH, "4.50"));
        
        leftMotor = PilotProps.getMotor(pp.getProperty(
        		PilotProps.KEY_LEFTMOTOR, "A"));
        
        rightMotor = PilotProps.getMotor(pp.getProperty(
        		PilotProps.KEY_RIGHTMOTOR, "C"));
        
        boolean reverse = Boolean.parseBoolean(pp.getProperty(
        		PilotProps.KEY_REVERSE,"true"));
        
        pilot =  new DifferentialPilot(wheelDiameter, trackWidth, leftMotor, 
        		rightMotor, reverse);
        
        
        leftMotor.resetTachoCount();
        rightMotor.resetTachoCount();

        heading = 0;
        XTach = 0;
        YTach = 0;
        
	}
	
	
	//Execute a left rotation to move around obstacle or rotate to another
	//search line.  Also updates the xy coordinates of the 
	public void executeLeftRotation() {
		nc.setXPos(nc.getXPos() + getTachCounts());
		moveDistance(-360);
		pilot.rotate(90);
		resetTachCount();
		
		moveDistance(540);
		nc.setYPos(nc.getYPos() + getTachCounts());
		pilot.rotate(-90);
		resetTachCount();
		headLeft = false;
		
	}
	
	public void executeRightRotation() {
		nc.setXPos(nc.getXPos() - getTachCounts());
		moveDistance(-360);
		pilot.rotate(-90);
		resetTachCount();
		
		moveDistance(540);
		nc.setYPos(nc.getYPos() + getTachCounts());
		pilot.rotate(90);
		headLeft = true;
		
	}
	
	public void backupAndRotate(String direction) {
		if (direction == "left") {
			
		} 
		
		else {
			
		}
	}
	
	//Allows direct access to both left and right motors for moving forwards
	//and backwards over a specific distance.  Saves on specific method calls,
	//honestly should be implemented in the Lejos API
	private void moveDistance(int rotationDistance) {
		leftMotor.rotate(rotationDistance);
		rightMotor.rotate(rotationDistance);
	}
	
	private int getTachCounts() {
		return (int)(leftMotor.getTachoCount() + rightMotor.getTachoCount()) / 2;
	}
	
	private void resetTachCount() {
		leftMotor.resetTachoCount();
		rightMotor.resetTachoCount();
	}
	
	//setters and getters.  May need to revisit this and re-evaluate what
	//is appropriate access
	public int getWheelSpeed() {
		return wheelSpeed;
	}

	public void setWheelSpeed(int wheelSpeed) {
		PilotController.wheelSpeed = wheelSpeed;
	}

	public int getTravelDistance() {
		return travelDistance;
	}

	public void setTravelDistance(int travelDistance) {
		PilotController.travelDistance = travelDistance;
	}

	public boolean getGoalNotFound() {
		return goalNotFound;
	}

	public void setGoalNotFound(boolean goalNotFound) {
		PilotController.goalNotFound = goalNotFound;
	}

	public boolean getHeadLeft() {
		return headLeft;
	}

	public void setHeadLeft(boolean headLeft) {
		PilotController.headLeft = headLeft;
	}
	
	public double getLeftTach() {
		return leftMotor.getTachoCount();
	}
	
	public double getRightTach(){
		return rightMotor.getTachoCount();
	}

	public DifferentialPilot getPilot(){
		return pilot;
	}
	
	
	
	
}
