/*	Startup behavior of sumo.  Requires a 5 second delay on startup, and drive
 * 	forward until contact has been made.
 * 
 */


package com.cos399.sumo.behaviors;

import com.cos399.sumo.behaviors.Actions;
import com.cos399.sumo.utils.SumoDataExchange;

import lejos.nxt.LCD;
import lejos.robotics.subsumption.Behavior;
import lejos.util.Delay;

public class Startup implements Behavior {

	SumoDataExchange de = SumoDataExchange.getInstance();
	
	@Override
	public boolean takeControl() {
		return SumoDataExchange.getInstance().getIsStart();
	}

	@Override
	public void action() {
		LCD.clear(0);
		LCD.drawString("Startup Action", 0, 0);
		
		Delay.msDelay(5000);
		SumoDataExchange.getInstance().setIsStart(false);
		Actions.getInstance().attack();
	}

	@Override
	public void suppress() {
	}

}
