/*	Testing class for Ultrasonic Sensor.  Assumes that sensor is mapped to 
 * 	Port S4 on the robot
 * 
 */

package com.cos399.test;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;

import lejos.nxt.MotorPort;
import lejos.nxt.NXTRegulatedMotor;



public class UltrasonicTester implements SensorTestInterface{

	private static UltrasonicTester tester = null;
	private UltrasonicSensor sensor;
	private NXTRegulatedMotor motor;
	
	private int sensorMotorSpeed = 30;
	private int sensorRotateAngle = 90;
	
	private UltrasonicTester() {}
	
	public static UltrasonicTester getInstance(){
		if (tester == null) {
			tester = new UltrasonicTester();
			tester.motor = new NXTRegulatedMotor(MotorPort.B);
			tester.motor.setSpeed(tester.sensorMotorSpeed);
			tester.sensor = new UltrasonicSensor(SensorPort.S4);
			tester.sensor.setMode(2);
			
			LCD.clear();
			LCD.drawString("Ultrasonic Testing", 0, 0);
			LCD.drawString("Press Escape\n to exit", 0, 1);
		}
		
		return tester;
	}
	
	public boolean testSensors() {
		LCD.drawString("Distance: ", 0, 3);
		LCD.drawString("Range: ", 0, 5);
		while (!Button.ESCAPE.isDown()){
			motor.rotate(sensorRotateAngle);
			LCD.clear(4);
			LCD.clear(6);
			LCD.drawString(Integer.toString(sensor.getDistance()), 0, 4);
			LCD.drawString(Float.toString(sensor.getRange()), 0, 6);
			motor.rotate(-sensorRotateAngle*2);

		}
		return false;
	}
	
	

	public static void main(String[] args) {


	}

}
